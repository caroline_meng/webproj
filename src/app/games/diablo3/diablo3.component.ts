import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { AngularFire, FirebaseListObservable, AuthProviders, AuthMethods} from 'angularfire2';
import { LoginService } from 'app/login/login.service';
import * as firebase from "firebase";


@Component({
  selector: 'app-diablo3',
  templateUrl: './diablo3.component.html',
  styleUrls: ['./diablo3.component.css'],
  providers: [LoginService]
})
export class Diablo3Component implements OnInit {
    public players: FirebaseListObservable<any[]>;
    public error: any;
    public isLoggedIn: boolean;
    public isVerified: boolean;
    public name: any;
    public games: FirebaseListObservable<any[]>;
    public users: FirebaseListObservable<any[]>;
    public isGame: boolean;
    public key;
    public db;
    public updateUser;
  
  constructor(private af: AngularFire, private router: Router, public loginService: LoginService) {
      this.games = af.database.list('/games');
      this.players = af.database.list('Diablo3');
      this.users = af.database.list('registeredUsers');
      
      // AUTHENTICATION--------------------------------------------------------------------------------
      this.loginService.af.auth.subscribe(
        (auth) => {
          if(auth) {
            //this.name = auth;
            console.log("Logged in.");
          //this.router.navigate(['login']);
            this.isLoggedIn = true;
            
            if(auth.google) {
              this.loginService.email = auth.google.email;
            }
            else {
            this.loginService.email = auth.auth.email;
           }
            
            
            if(!firebase.auth().currentUser.emailVerified)
              {
                console.log("not verified");
                  this.isVerified = false;
              }
            else{console.log("verified"); this.isVerified = true;}
        }
        else {
            console.log("Not Logged in.");
            this.isLoggedIn = false;
        }
          
        })
  // CHECK IF PLAYER HAS GAME--------------------------------------------------------------------------------
      this.users.forEach(element => {
       element.forEach(item=>{
         if (item.userEmail==this.loginService.email){
           if(!item.userPlaylist.Diablo3){
            this.key=item.$key;
            console.log(this.key);
            this.isGame = false;
          } 
         }});
      });
      this.db= 'https://teamfinder-1fdd3.firebaseio.com/registeredUsers/'
      console.log(this.db)
      this.updateUser=af.database.list(this.db);
      //console.log(this.updateUser);
  }

  AddGame(){
    //console.log(this.updateUser);
    this.isGame = true;
    this.updateUser.update(this.key+'/userPlaylist', {Diablo3: true});
  }

  
       

  ngOnInit() {
  }


}
