import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Diablo3Component } from './diablo3.component';

describe('Diablo3Component', () => {
  let component: Diablo3Component;
  let fixture: ComponentFixture<Diablo3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Diablo3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Diablo3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
