import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldofwarcraftComponent } from './worldofwarcraft.component';

describe('WorldofwarcraftComponent', () => {
  let component: WorldofwarcraftComponent;
  let fixture: ComponentFixture<WorldofwarcraftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldofwarcraftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldofwarcraftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
