import { RouterModule, Router,Routes } from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';

import { FrontpgComponent } from './frontpg/frontpg.component';
import { LoginComponent } from './login/login.component';
import { RegisteComponent } from './registe/registe.component';
import { PgnotfoundComponent } from './pgnotfound/pgnotfound.component';
import { ProfileComponent} from './profile/profile.component';
import { GamesComponent } from './games/games.component';
import { LfgComponent } from './lfg/lfg.component';
import { RequestComponent } from './request/request.component';
import { MyrequestsComponent } from './myrequests/myrequests.component';
import { DestinyComponent } from './games/destiny/destiny.component';
import { Diablo3Component } from './games/diablo3/diablo3.component';
import { Left4dead2Component } from './games/left4dead2/left4dead2.component';
import { WorldofwarcraftComponent } from './games/worldofwarcraft/worldofwarcraft.component';
import { FilterComponent } from './lfg/filter/filter.component';

const appRoutes: Routes = [
   { path: '', component: FrontpgComponent},
   { path: 'login', component: LoginComponent},
   { path: 'register', component: RegisteComponent},
   { path: 'notfound', component: PgnotfoundComponent},
   { path: 'profile', component: ProfileComponent},
   { path: 'games', component: GamesComponent},
   { path: 'lfg', component: LfgComponent},
   { path: 'lfg/request', component: RequestComponent},
   { path: 'lfg/myrequests', component: MyrequestsComponent},
   { path: 'games/destiny', component: DestinyComponent},
   { path: 'games/diablo3', component: Diablo3Component},
   { path: 'games/left4dead2', component: Left4dead2Component},
   { path: 'games/worldofwarcraft', component: WorldofwarcraftComponent},
   { path: 'lfg/filter', component: FilterComponent}
   
]

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

