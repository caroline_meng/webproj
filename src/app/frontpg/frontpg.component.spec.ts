import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontpgComponent } from './frontpg.component';

describe('FrontpgComponent', () => {
  let component: FrontpgComponent;
  let fixture: ComponentFixture<FrontpgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontpgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontpgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
