import { Component, OnInit } from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';
import { LoginService } from 'app/login/login.service';
import {Router} from "@angular/router";


@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css'],
  providers: [LoginService]
})
export class RequestComponent implements OnInit {
  
  public messages: FirebaseListObservable<any>;

  constructor(public af: AngularFire, public loginService: LoginService, private router: Router) { 
        this.messages = this.af.database.list('messages');
        
        this.loginService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {

        }
        else {
          // Set the Display Name and Email so we can attribute messages to them
          if(auth.google) {
            this.loginService.email = auth.google.email;
            //this.userEmail=this.loginService.email;
          }
          else {
            this.loginService.email = auth.auth.email;
            //console.log(this.loginService.displayName);
            //this.userEmail=this.loginService.email;
          }
        }
      }
    );
  
  }

  ngOnInit() {
  }
  
  sendMessage(games, platforms, type, match, starttime, hours, min, newmessage, userEmail ) {
    console.log(games, platforms, type, match, starttime, hours, min, newmessage);
    if ((new Date(starttime).getTime() / 1000) < Date.now()){
      console.log("undefined");
    }
    var message = {
      timestamp: Date.now(),
      gameReq: games,
      platformReq: platforms,
      typesReq: type,
      matchReq: match,
      starttimeReq: starttime,
      hoursReq: hours,
      minReq: min,
      messageReq: newmessage,
      userEmail: userEmail
    };
    console.log(message);
    this.messages.push(message);
    console.log("message saved to db!");
    this.router.navigate(["lfg"]);
  }

}
