import { Component, OnInit,NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { FormControl, FormGroup,Validators,FormBuilder } from '@angular/forms';
import { LoginService } from './login.service';
import {Router} from "@angular/router";
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import * as firebase from "firebase";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})

@NgModule({
 imports: [ FormsModule ]
})

export class LoginComponent implements OnInit {
    public error: any;
    public userName;

    constructor(private router: Router,public loginService: LoginService, public af: AngularFire) {
      //this.firebaseAuth = _AngularFire.auth;
    }

    ngOnInit() {
    }
    
    //firebaseAuth: AngularFireAuth;

  
  loginWithGoogle(){
    this.loginService.loginWithGoogle().then((data) =>{
    console.log(data);
    this.userName = data.auth.displayName;
    console.log(this.userName);
    this.router.navigate(['']);
    });
  }
  
  loginWithEmail(event, email, password){
    event.preventDefault();
    this.loginService.loginWithEmail(email, password).then(() => {
      this.router.navigate(['']);
    })
      .catch((error: any) => {
        if (error) {
          this.error = error;
          console.log(this.error);
        }
      });
  }
   
      
    resetPassword(emailr) {
    console.log("reset button ok");
    firebase.auth().sendPasswordResetEmail(emailr).then(function() {
      // Email sent.
      console.log("success");
    }, function(error) {
      // An error happened.
      console.log("error" + error);
    });
  }
   
  
}
