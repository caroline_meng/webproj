"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var angularfire2_1 = require('angularfire2');
var LoginService = (function () {
    //public user: FirebaseObjectObservable<any>;
    function LoginService(af) {
        this.af = af;
    }
    /**
   * Logs in the user
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
    LoginService.prototype.loginWithGoogle = function () {
        return this.af.auth.login({
            provider: angularfire2_1.AuthProviders.Google,
            method: angularfire2_1.AuthMethods.Popup
        });
    };
    /**
     * Logs out the current user
     */
    LoginService.prototype.logout = function () {
        return this.af.auth.logout();
    };
    /**
     * Calls the AngularFire2 service to register a new user
     * @param model
     * @returns {firebase.Promise<void>}
     */
    LoginService.prototype.registerUser = function (email, password) {
        console.log(email);
        return this.af.auth.createUser({
            email: email,
            password: password
        });
    };
    /**
     * Saves information to display to screen when user is logged in
     * @param uid
     * @param model
     * @returns {firebase.Promise<void>}
     */
    LoginService.prototype.saveUserInfoFromForm = function (uid, name, email) {
        return this.af.database.object('registeredUsers/' + uid).set({
            name: name,
            email: email
        });
    };
    /**
    * Logs the user in using their Email/Password combo
    * @param email
    * @param password
    * @returns {firebase.Promise<FirebaseAuthState>}
    */
    LoginService.prototype.loginWithEmail = function (email, password) {
        return this.af.auth.login({
            email: email,
            password: password
        }, {
            provider: angularfire2_1.AuthProviders.Password,
            method: angularfire2_1.AuthMethods.Password
        });
    };
    LoginService = __decorate([
        core_1.Injectable()
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
