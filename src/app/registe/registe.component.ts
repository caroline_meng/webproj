import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { FormControl, FormGroup,Validators,FormBuilder } from '@angular/forms';
//import { LoginService } from 'app/login/login.service';
import {Router} from "@angular/router";
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

@Component({
  selector: 'app-registe',
  templateUrl: './registe.component.html',
  styleUrls: ['./registe.component.css']
   // providers: [LoginService]
})
export class RegisteComponent implements OnInit {

  state: string = '';
  error: any;


  constructor(public af: AngularFire,private router: Router) {
    //.user = this.af.database.list('registeredUsers');
  }

  ngOnInit() {
  }
  onSubmit(formData) {
    if(formData.valid) {
    console.log(formData.value);
    this.af.auth.createUser({
    email: formData.value.email,
    password: formData.value.password
  }).then(
    authState => {
    authState.auth.sendEmailVerification();
    this.router.navigate(['/'])
  }).catch(
    (err) => {
    console.log(err);
    this.error = err;
  })
}
}
}

