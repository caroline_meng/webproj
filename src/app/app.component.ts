import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { LoginService } from 'app/login/login.service';
import * as firebase from "firebase";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [LoginService]
})
export class AppComponent {
      public error: any;
      public isLoggedIn: boolean;
      public isVerified: boolean;
      public name: any;
       
    constructor(public af: AngularFire,private router: Router, public loginService: LoginService,) {
      this.loginService.af.auth.subscribe(
        (auth) => {
          if(auth) {
            //this.name = auth;
            console.log("Logged in.");
          //this.router.navigate(['login']);
            this.isLoggedIn = true;
            
            if(auth.google) {
              this.loginService.displayName = auth.google.displayName;
            }
            else {
            this.loginService.displayName = auth.auth.email;
           }
            
            
            if(!firebase.auth().currentUser.emailVerified)
              {
                console.log("not verified");
                  this.isVerified = false;
              }
            else{console.log("verified"); this.isVerified = true;}
        }
        else {
            console.log("Not Logged in.");
            this.isLoggedIn = false;
        }
          
        })
}   
    logout() {
      this.loginService.logout();
      this.router.navigate(['/login']);
  }
}