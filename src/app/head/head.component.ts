import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';


@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.css']
})
export class HeadComponent implements OnInit {
  
  users: FirebaseListObservable<any[]>;
  count;
  userCount;

  title = 'Welcome to Your Online Community for Multiplayer Matchups!';
  intro = "Please get started by logging into an existing account or register ~";
  purpose = "Most games have online multiplayer modes with cooperative or adversarial play. Finding good teammates that you enjoy playing with can be a challenge. Finding new people to play with or against who share your playing style can enhance your multiplayer experience. This app will help you connect with other players who share your play style, platforms and games.";
  
  constructor(af: AngularFire) {
    this.users = af.database.list('/registeredUsers');
    this.users.subscribe((res) => {
    this.count = res;
    console.log(this.count.length);
    this.userCount = this.count.length
    })
  }

  ngOnInit() {
  }

}
