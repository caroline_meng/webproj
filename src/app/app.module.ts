import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing, appRoutingProviders  } from './app.routing';
import { AngularFireModule } from 'angularfire2';
import { O2UploadToFbsComponent } from 'o2-upload-to-fbs';	

import { AppComponent } from './app.component';
import { RegisteComponent } from './registe/registe.component';
import { CommercialComponent } from './commercial/commercial.component';
import { HeadComponent } from './head/head.component';
import { FrontpgComponent } from './frontpg/frontpg.component';
import { LoginComponent } from './login/login.component';
import { PgnotfoundComponent } from './pgnotfound/pgnotfound.component';
import { GamesComponent } from './games/games.component';
import { ProfileComponent } from './profile/profile.component';
import { LfgComponent } from './lfg/lfg.component';
import { RequestComponent } from './request/request.component';
import { MyrequestsComponent } from './myrequests/myrequests.component';
import { DestinyComponent } from './games/destiny/destiny.component';
import { Diablo3Component } from './games/diablo3/diablo3.component';
import { Left4dead2Component } from './games/left4dead2/left4dead2.component';
import { WorldofwarcraftComponent } from './games/worldofwarcraft/worldofwarcraft.component';
import { FilterComponent } from './lfg/filter/filter.component';



export const firebaseConfig = {
    apiKey: "AIzaSyB82TxZoBAXJMF76hd57auqbDP8ruoEN2A",
    authDomain: "teamfinder-1fdd3.firebaseapp.com",
    databaseURL: "https://teamfinder-1fdd3.firebaseio.com",
    storageBucket: "teamfinder-1fdd3.appspot.com",
    messagingSenderId: "3579307170"
};

@NgModule({
  declarations: [
    AppComponent,
    RegisteComponent,
    CommercialComponent,
    HeadComponent,
    FrontpgComponent,
    LoginComponent,
    PgnotfoundComponent,
    GamesComponent,
    ProfileComponent,
    LfgComponent,
    RequestComponent,
    O2UploadToFbsComponent,
    MyrequestsComponent,
    DestinyComponent,
    Diablo3Component,
    Left4dead2Component,
    WorldofwarcraftComponent,
    FilterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    AngularFireModule.initializeApp(firebaseConfig),
  ],
  providers: [
    appRoutingProviders,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
