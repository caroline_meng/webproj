import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable, AuthProviders, AuthMethods } from 'angularfire2';
import { Observable } from 'rxjs';
import * as firebase from "firebase";
import { ProfileService } from './profile.service';
import { LoginService } from 'app/login/login.service';
import {Router} from "@angular/router";

interface Image {
    path: string;
    filename: string;
    downloadURL?: string;
    $key?: string;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ProfileService, LoginService]
})
export class ProfileComponent implements OnInit {
    
    constructor(public af: AngularFire, private profileService: ProfileService, public loginService: LoginService, private router: Router) {
      this.loginService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {

        }
        else {
          // Set the Display Name and Email so we can attribute messages to them
          if(auth.google) {
            this.loginService.email = auth.google.email;
            //this.userEmail=this.loginService.email;
          }
          else {
            this.loginService.email = auth.auth.email;
            //console.log(this.loginService.displayName);
            //this.userEmail=this.loginService.email;
          }
        }
      }
    );
        
    }
    ngOnInit() {
        
    }

  
    //PROFILE IMAGE--------------------------------------------------------------------------------
    imgUrl: string;
    
    fileList : FirebaseListObservable<Image[]>;
    imageList : Observable<Image[]>;
    
    public isImg: boolean;

    
    noImg(){
      this.isImg = false;
      this.imgUrl = "https://firebasestorage.googleapis.com/v0/b/teamfinder-1fdd3.appspot.com/o/user1.png?alt=media&token=5d12a41c-424f-45fa-bb76-b6e090c868d1";
    }
    
    upload() {
        // Create a root reference
        let storageRef = firebase.storage().ref();

        let success = false;
        // This currently only grabs item 0, TODO refactor it to grab them all
        for (let selectedFile of [(<HTMLInputElement>document.getElementById('file')).files[0]]) {
            console.log(selectedFile);
            if (selectedFile.size< 10000){
                break;
            }
            // Make local copies of services because "this" will be clobbered
            let af = this.af;
            //let folder = "userImg";
            let path = `/userImg/${selectedFile.name}`;
            var iRef = storageRef.child(path);
            iRef.put(selectedFile).then((snapshot) => {
                console.log("Img Uploaded!");
                af.database.list(`/userImg/images/`).push({ path: path, filename: selectedFile.name });
                let downloadUrl = snapshot.downloadURL;
                console.log(downloadUrl);
                this.imgUrl = downloadUrl;
                this.isImg = true;
            });
        }
    
    }

//PROFILE & GAME INFORMATION-----------------------------------------------------------------

    playtimes = ['0-8am', '8am-12am', '12am-8pm','8pm-12pm'];
  timezones = ['UTC -7','UTC -6','UTC -5','UTC -4','UTC -3','UTC -2:30'];
  microphones = ['PC','PS3','PS4','XBox','XB1'];
  pcgames=['Left4Dead2','WorldofWarcraft'];
  ps3games=['Destiny','Diablo3'];
  ps4games=['Destiny','Diablo3'];
  xboxgames=['Destiny','Diablo3','Left4Dead2'];
  xb1games=['Destiny','Diablo3'];
  playstyles=['Any','Casual','Serious','Zealous'];
  destinycharacters=['Hunter','Warlock','Titan'];
  diablo3characters=['Barbarian','Crusader','Demon Hunter','Monk','Witch','Doctor','Wizard'];
  left4dead2characters=['coach','Rochelle','Nick','Ellis'];
  worldofwarcraftcharacters=['Warrior','Paladin','Hunter','Rogue','Priest',
              'Death Knight','Shaman','Mage','Warlock','Monk','Druid','Demon Knight'];
  playlists = ['Destiny','Diablo3','Left4Dead2','WorldofWarcraft' ];
              
  playtimeWeekdays = {
        '0-8am': true,
        '8am-12am': true,
        '12am-8pm':true,
        '8pm-12pm':true
  };
  playtimeWeekends = {
        '0-8am': true,
        '8am-12am': true,
        '12am-8pm': true,
        '8pm-12pm':true
  };
  timezoneWeekdays={
    'UTC -7':true,
    'UTC -6':true,
    'UTC -5':true,
    'UTC -4':true,
    'UTC -3':true,
    'UTC -2:30':true
  };
  timezoneWeekends={
    'UTC -7':true,
    'UTC -6':true,
    'UTC -5':true,
    'UTC -4':true,
    'UTC -3':true,
    'UTC -2:30':true
  };
  
  microphoneStus={
    'PC':true,
    'PS3':true,
    'PS4':true,
    'XBox':true,
    'XB1':true
  };
  pcgameStus={
    'Left4Dead2':true,
    'WorldofWarcraft':true
  };
  ps3gameStus={
    'Destiny':true,
    'Diablo3':true
  };
  ps4gameStus={
    'Destiny':true,
    'Diablo3':true
  };
  xboxgameStus={
    'Destiny':true,
    'Diablo3':true,
    'Left4Dead2':true,
  }
  xb1gameStus={
    'Destiny':true,
    'Diablo3':true
  };
  destinycharacterStus={
    'Hunter':true,
    'Warlock':true,
    'Titan':true
  };
  diablo3characterStus={
    'Barbarian':true,
    'Crusader':true,
    'Demon Hunter':true,
    'Monk':true,
    'Witch':true,
    'Doctor':true,
    'Wizard':true
  };
  left4dead2characterStus={
    'coach':true,
    'Rochelle':true,
    'Nick':true,
    'Ellis':true
  };
  worldofwarcraftcharacterStus={
    'Warrior':true,
    'Paladin': true,
    'Hunter':true,
    'Rogue':true,
    'Priest':true,
    'Death Knight':true,
    'Shaman':true,
    'Mage':true,
    'Warlock':true,
    'Monk':true,
    'Druid':true,
    'Demon Knight':true
  }

playlistStus={
    'Destiny':true,
    'Diablo3':true,
    'Left4Dead2':true,
    'WorldofWarcraft':true
}
 
  
  updateCheckedPlaytimesWeekdays(playtime,playtimeWeekdays,event){
    this.profileService.updateCheckedOptions(playtime,playtimeWeekdays,event);
  }
  updateCheckedPlaytimesWeekends(playtime,playtimeWeekends,event){
    this.profileService.updateCheckedOptions(playtime,playtimeWeekends,event);
  }
  
  
  TimezoneWeekdays(timezone,timezoneWeekdays,event){
    this.profileService.updateCheckedOptions(timezone,timezoneWeekdays,event);
  }
  TimezoneWeekends(timezone,timezoneWeekends,event){
    this.profileService.updateCheckedOptions(timezone,timezoneWeekends,event);
  }
  
  
  MicrophoneStus(microphone,microphoneStus,event){
    this.profileService.updateCheckedOptions(microphone,microphoneStus,event);
  }
  
  
  PcgameStus(pcgame,pcgameStus,event){
    this.profileService.updateCheckedOptions(pcgame,pcgameStus,event);
  }
  Ps3gameStus(ps3game,ps3gameStus,event){
    this.profileService.updateCheckedOptions(ps3game,ps3gameStus,event);
  }
  Ps4gameStus(ps4game,ps4gameStus,event){
    this.profileService.updateCheckedOptions(ps4game,ps4gameStus,event);
  }
  XboxgameStus(xboxgame,xboxgameStus,event){
    this.profileService.updateCheckedOptions(xboxgame,xboxgameStus,event);
  }
  Xb1gameStus(xb1game,xb1gameStus,event){
    this.profileService.updateCheckedOptions(xb1game,xb1gameStus,event);
  }
  
  
  DestinycharacterStus(destinycharacter,destinycharacterStus,event){
    this.profileService.updateCheckedOptions(destinycharacter,destinycharacterStus,event);
  }
  Diablo3characterStus(diablo3character,diablo3characterStus,event){
    this.profileService.updateCheckedOptions(diablo3character,diablo3characterStus,event);
  }
  Left4Dead2characterStus(left4dead2character,left4dead2characterStus,event){
    this.profileService.updateCheckedOptions(left4dead2character,left4dead2characterStus,event);
  }
  WorldofWarcraftcharacterStus(worldofwarcraftcharacter,worldofwarcraftcharacterStus,event){
    this.profileService.updateCheckedOptions(worldofwarcraftcharacter,worldofwarcraftcharacterStus,event);
  }
  
  UpdatePlaylist(playlist,playlistStus,event){
    this.profileService.updateCheckedOptions(playlist,playlistStus,event);
  }

  
  sendProfile(age,idpc,idps3,idps4,idxbox,idxb1,
  playtimeWeekdays,  playtimeWeekends,timezoneWeekdays,timezoneWeekends,
  microphoneStus,pcgameStus,ps3gameStus,ps4gameStus,xboxgameStus,xb1gameStus,
  DestinyPlayStyle,Diabl3PlayStyle,Left4Dead2PlayStyle,WorldofWarcraftPlayStyle,
  destinycharacterStus,diablo3characterStus,left4dead2characterStus,worldofwarcraftcharacterStus,imgUrl,userEmail,playlistgames
  
  ){
    this.profileService.sendProfile(age,idpc,idps3,idps4,
    idxbox,idxb1,playtimeWeekdays,  playtimeWeekends,timezoneWeekdays,
    timezoneWeekends,microphoneStus,pcgameStus,ps3gameStus,ps4gameStus,
    xboxgameStus,xb1gameStus,DestinyPlayStyle,Diabl3PlayStyle,Left4Dead2PlayStyle,
    WorldofWarcraftPlayStyle,destinycharacterStus,diablo3characterStus,
    left4dead2characterStus,worldofwarcraftcharacterStus,imgUrl,userEmail,playlistgames
    );
    console.log("PROFILE CREATED!");
    this.router.navigate([""]);
  }
    

}



