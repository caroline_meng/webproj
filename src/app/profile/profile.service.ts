import { Injectable } from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';

@Injectable()
export class ProfileService {
  public profiles: FirebaseListObservable<any>;
  public destiny: FirebaseListObservable<any>;
  public diablo3: FirebaseListObservable<any>;
  public left4dead2: FirebaseListObservable<any>;
  public worldofwarcraft: FirebaseListObservable<any>;

  constructor(public af: AngularFire) { 
      this.profiles = this.af.database.list('/registeredUsers');
      this.destiny = this.af.database.list('Destiny');
      this.diablo3 = this.af.database.list('Diablo3');
      this.left4dead2 = this.af.database.list('Left4Dead2');
      this.worldofwarcraft = this.af.database.list('WorldofWarcraft');
  }
     

    
    updateCheckedOptions(option, optionsMap, event) {
        optionsMap[option] = event.target.checked;
    }
    
    
  sendProfile(age,idpc,idps3,idps4,idxbox,idxb1,
  playtimeWeekdays,playtimeWeekends,timezoneWeekdays,timezoneWeekends,
  microphoneStus,pcgameStus,ps3gameStus,ps4gameStus,xboxgameStus,xb1gameStus,
  DestinyPlayStyle,Diabl3PlayStyle,Left4Dead2PlayStyle,WorldofWarcraftPlayStyle,
  destinycharacterStus,diablo3characterStus,left4dead2characterStus,
  worldofwarcraftcharacterStus, imgUrl, userEmail,playlistgames
  ) {
       
       //GAMES DATABASE TO GET #PLAYERS, IDS
        var destinyCnt={
        useridPS3: idps3,
        useridPS4: idps4,
        useridXBox: idxbox,
        useridXB1: idxb1,
        userEmail:userEmail
    };
    var diablo3Cnt={
        useridPS3: idps3,
        useridPS4: idps4,
        useridXBox: idxbox,
        useridXB1: idxb1,
        userEmail:userEmail
    };
    var left4dead2Cnt={
        useridPC: idpc,
        useridXBox: idxbox,
        userEmail:userEmail
    };
    var worldofwarcraftCnt={
        useridPC: idpc,
        userEmail:userEmail
    };
    
    if (playlistgames.Destiny ){
        this.destiny.push(destinyCnt);
    }
    if (playlistgames.Diablo3 ){
        this.diablo3.push(diablo3Cnt);
    }
    if (playlistgames.Left4Dead2 ){
        this.left4dead2.push(left4dead2Cnt);
    }
    if (playlistgames.WorldofWarcraft ){
        this.worldofwarcraft.push(worldofwarcraftCnt);
    }
      
     //USER FULL PROFILE PUSHING INTO DATABASE---------------------------------------- 
    var profile = {
      userage: age,
      idpc: idpc,
      idps3: idps3,
      idps4: idps4,
      idxbox: idxbox,
      idxb1: idxb1,
      userplaytimeWeekdays: playtimeWeekdays,
      userplaytimeWeekends:  playtimeWeekends,
      usertimezoneWeekdays: timezoneWeekdays,
      usertimezoneWeekends: timezoneWeekends,
      usermicrophoneStus: microphoneStus,
      userpcgameStus: pcgameStus,
      userps3gameStus: ps3gameStus,
      userps4gameStus: ps4gameStus,
      userxboxgameStus: xboxgameStus,
      userxb1gameStus: xb1gameStus,
      userDestinyPlayStyle: DestinyPlayStyle,
      userDiabl3PlayStyle: Diabl3PlayStyle,
      userLeft4Dead2PlayStyle: Left4Dead2PlayStyle,
      userWorldofWarcraftPlayStyle: WorldofWarcraftPlayStyle,
      userDestinycharacterStus: destinycharacterStus,
      userDiablo3characterStus: diablo3characterStus,
      userLeft4dead2characterStus:left4dead2characterStus,
      userWorldofwarcraftcharacterStus:worldofwarcraftcharacterStus,
    userimgUrl: imgUrl,
    useEmail: userEmail,
    userPlaylist: playlistgames

    };
    console.log(profile);
    this.profiles.push(profile);
  }
}
