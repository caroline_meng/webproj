import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';
import { LoginService } from 'app/login/login.service';
import * as firebase from "firebase";


@Component({
  selector: 'app-lfg',
  templateUrl: './lfg.component.html',
  styleUrls: ['./lfg.component.css'],
  providers: [LoginService]
})


export class LfgComponent implements OnInit {


     
      public error: any;
      public isLoggedIn: boolean;
      public isVerified: boolean;
      public name: any;
      
      public messages: FirebaseListObservable<any>;
      public pastStarts: FirebaseListObservable<any>;
      public nowTime: number = Date.now(); 
      
    constructor(public af: AngularFire,private router: Router, public loginService: LoginService,) {
      this.messages = this.af.database.list('messages');
      
      this.loginService.af.auth.subscribe(
        (auth) => {
          if(auth) {
            //this.name = auth;
            //console.log("Logged in.");
          //this.router.navigate(['login']);
            this.isLoggedIn = true;
            
            if(auth.google) {
              this.loginService.displayName = auth.google.displayName;
            }
            else {
            this.loginService.displayName = auth.auth.email;
           }
            
            
            if(!firebase.auth().currentUser.emailVerified)
              {
                console.log("not verified");
                  this.isVerified = false;
              }
            else{console.log("verified"); this.isVerified = true;}
        }
        else {
            console.log("Not Logged in.");
            this.isLoggedIn = false;
        }
          
        })
      
      // ------------ NO SHOW OF PAST START TIME POST
      this.pastStarts = this.af.database.list('messages',{
      query:{
        orderByChild: 'starttimeReq',
        startAt: this.nowTime
      }
    });
}   

  

  ngOnInit() {
  }



}
