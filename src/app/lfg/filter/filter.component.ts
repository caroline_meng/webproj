import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';
import { LoginService } from 'app/login/login.service';
import * as firebase from "firebase";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
  providers: [LoginService]
})
export class FilterComponent implements OnInit {
      public messages: FirebaseListObservable<any>;
      public pastStarts: FirebaseListObservable<any>;
      public frequests;
      public rgame;
      public rplatform;
      public rtime;
      
      public error: any;
      public isLoggedIn: boolean;
      public isVerified: boolean;
  
    constructor(public af: AngularFire,private router: Router, public loginService: LoginService,) {
      this.messages = this.af.database.list('messages');
      
      this.loginService.af.auth.subscribe(
        (auth) => {
          if(auth) {
            //this.name = auth;
            //console.log("Logged in.");
          //this.router.navigate(['login']);
            this.isLoggedIn = true;
            
            if(auth.google) {
              this.loginService.displayName = auth.google.displayName;
            }
            else {
            this.loginService.displayName = auth.auth.email;
           }
            
            
            if(!firebase.auth().currentUser.emailVerified)
              {
                console.log("not verified");
                  this.isVerified = false;
              }
            else{console.log("verified"); this.isVerified = true;}
        }
        else {
            console.log("Not Logged in.");
            this.isLoggedIn = false;
        }
          
        })
      
    }
    
    filtergame(game){
    this.rgame=game;
    this.frequests = this.af.database.list('messages',{
      query:{
        orderByChild: 'gameReq',
        equalTo: this.rgame
      }
    });
    }
    
    filterplatform(platform){
    this.rplatform=platform;
    this.frequests = this.af.database.list('messages',{
      query:{
        orderByChild: 'platformReq',
        equalTo: this.rplatform
      }
    });
    }
    
    filtertime(ftime){
      this.rtime=ftime;
      this.frequests = this.af.database.list('messages',{
        query:{
        orderByChild: 'starttimeReq',
        startAt: this.rtime
      }
    });
    }
    combofilter(cgame, cplatform){
      this.rgame = cgame; 
      this.rplatform = cplatform;
      this.frequests = this.messages.filter(message => (message.gameReq==this.rgame&& message.platformReq == this.rplatform));
    }

    
  ngOnInit() {
  }

}
