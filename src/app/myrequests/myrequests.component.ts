import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { AngularFire, FirebaseListObservable, AuthProviders, AuthMethods} from 'angularfire2';
import { LoginService } from 'app/login/login.service';
import * as firebase from "firebase";


@Component({
  selector: 'app-myrequests',
  templateUrl: './myrequests.component.html',
  styleUrls: ['./myrequests.component.css'],
  providers: [LoginService]
})
export class MyrequestsComponent implements OnInit {

      public error: any;
      public isLoggedIn: boolean;
      public isVerified: boolean;
      public name: any;
      public key;
      public isRepost: boolean;
      public repostKey;
      public db;
      public nowTime;
      public updatePost;
      
      
      
      public messages: FirebaseListObservable<any>;
      //public nowTime: number = Date.now(); 
      
      public pastRequests: FirebaseListObservable<any>;
      
    constructor(public af: AngularFire,private router: Router, public loginService: LoginService,) {
      this.nowTime = Date.now();
      this.isRepost= false;
      this.messages = this.af.database.list('messages');
      
      this.loginService.af.auth.subscribe(
        (auth) => {
          if(auth) {
            //this.name = auth;
            //console.log("Logged in.");
          //this.router.navigate(['login']);
            this.isLoggedIn = true;
            
            if(auth.google) {
              this.loginService.email = auth.google.email;
            }
            else {
            this.loginService.email = auth.auth.email;
           }
            
            
            if(!firebase.auth().currentUser.emailVerified)
              {
                console.log("not verified");
                  this.isVerified = false;
              }
            else{console.log("verified"); this.isVerified = true;}
        }
        else {
            console.log("Not Logged in.");
            this.isLoggedIn = false;
        }
          
        })
      console.log(this.loginService.email);
      this.pastRequests = this.af.database.list('messages',{
      query:{
        orderByChild: 'userEmail',
        equalTo: this.loginService.email
      }
    });

      this.db= 'https://teamfinder-1fdd3.firebaseio.com/messages/'
      console.log(this.db)
      this.updatePost=af.database.list(this.db);
    }   

  deletePost(key){
    this.key = key;
    console.log("delete clicked");
    this.af.database.object('messages/'+ this.key).remove();
  }
  
  
  sendMessage(key, games, platforms, type, match, starttime, hours, min, newmessage, email){
    console.log("reposting works");
      this.repostKey= key;
      this.updatePost.update(this.repostKey, {
        gameReq: games,
        hoursReq: hours,
        matchReq: match,
        minReq: min,
        platformReq: platforms,
        timestamp: this.nowTime,
        typesReq: type,
        starttimeReq: starttime,
        messageReq: newmessage,
        userEmail: email
      });
  }
  
  ngOnInit() {
  }

}
